class Stop {
    start() {
        console.log('started!');
        return new Start();
    }
}


class Start {
    stop() {
        console.log('stoppped!');
        return new Stop();
    }
}


class Bus {
    constructor(state) {
        this.state = state;
        console.log('bus is initialized:', this.state)
    }

    getState() {
        return this.state;
    }

    setState(state) {
        this.state = state;
    }

    start() {
        this.setState(this.state.start())
    }

    stop() {
        this.setState(this.state.stop())
    }    
}


bus = new Bus(new Start());
console.log('curr state is:', bus.getState());

bus.setState(new Stop());
console.log('curr state is:', bus.getState());

bus.start()
console.log('curr state is:', bus.getState());

try{
    bus.start();
} 
catch(err) {
    console.log('action is not permitted', err)
}
class SpeakCurrStation {
    closeDoors() {
        console.log('\nclosed!');
        return new CloseDoors();
    }      
}


class SpeakNextStation {
    stop() {
        console.log('\nstopped!');
        return new CloseDoors();
    }      
}


class Drive {
    stop() {
        console.log('\nstopped!');
        return new Stop();
    }

    speakNextStation() {
        console.log('\nspeakNextStation!');
        return new SpeakNextStation();        
    }    
}


class Stop {
    drive() {
        console.log('\ndriveed!');
        return new Drive();
    }

    openDoors() {
        console.log('\nopened!');
        return new OpenDoors();
    }    

    speakCurrStation() {
        console.log('\nspeakCurrStation!');
        return new SpeakCurrStation();        
    }
}


class OpenDoors {   
    closeDoors() {
        console.log('closed!');
        return new CloseDoors();
    }   

    speakCurrStation() {
        console.log('\nspeakCurrStation!');
        return new SpeakCurrStation();        
    }    
}


class CloseDoors {
    openDoors() {
        console.log('\nopened!');
        return new OpenDoors();
    }

    drive() {
        console.log('\ndriveed!');
        return new Drive();
    }     
}


class Bus {
    constructor(state) {
        this.state = state;
        console.log('bus is initialized:', this.state)
    }

    getState() {
        return this.state;
    }

    setState(state) {
        this.state = state;
    }

    drive() {
        this.setState(this.state.drive())
    }

    stop() {
        this.setState(this.state.stop())
    }

    openDoors() {
        this.setState(this.state.openDoors())
    }    

    closeDoors() {
        this.setState(this.state.closeDoors())
    }    

    speakCurrStation() {
        this.setState(this.state.speakCurrStation())
    }

    speakNextStation() {
        this.setState(this.state.speakNextStation())
    }    
}


bus = new Bus(new Drive());
console.log('curr state is:', bus.getState());

bus.setState(new Stop());
console.log('curr state is:', bus.getState());

bus.openDoors()
console.log('curr state is:', bus.getState());

bus.speakCurrStation()
console.log('curr state is:', bus.getState());

bus.closeDoors()
console.log('curr state is:', bus.getState());

bus.drive()
console.log('curr state is:', bus.getState());

// must be error
// bus.openDoors()
// console.log('curr state is:', bus.getState());

bus.speakNextStation()
console.log('curr state is:', bus.getState());

